#!/usr/bin/perl
#reformat dac bits to proper SPI script format
#8 bit address and 32 bit data
#SPIWriteBye 0x90 0x2233
use Getopt::Long;
my $help = '';	# option variable with default value (false)
my $dec = '';	# option variable with default value (false), value is in dec
GetOptions ('help' => \$help, 'dec' => \$dec);

$USAGE="
prog CSR.csv BaseAddress 
(eg. reformatCSV2spi.pl RX_CSR.csv 0x00024000
output 
# SPI command to progress CSR
# Source file RX_CSR.csv
iwl 0x00014000 0x00080000
iwl 0x00014004 0x00000000
iwl 0x00014008 0x00000000
iwl 0x0001400c 0x00000000
iwl 0x00014010 0x00000000
iwl 0x00014014 0x00000000
iwl 0x00014018 0x00000000
iwl 0x0001401c 0x00000000
)
#########################
Block CSR	BaseAddress	
Bias		:	0x00020000
DAC		:	0x00021000
Alog		:	0x00023000
RX		:	0x00024000	
TX1		:	0x00025000	
TX2		:	0x00026000	
TX3		:	0x00027000	
TX4		:	0x00028000	
TX5		:	0x00029000	
TX6		:	0x0002A000	
SYN		:	0x0002B000	
ADC+FAAF	:	0x00030000
";

if($help){
	print "$USAGE \n";
	exit 0;
}
$infile=$ARGV[0];
$baseAddr=$ARGV[1];
$debug=0;
$colbyte=2;
$colVal=5;

print "### $dec \n";
open $fhin,'<',$infile || die "cant open $infile";
$SPICMD="iwl";
print "# SPI command to progress CSR\n";
print "# Source file $infile\n";
$bank=0;
$bankbit=0;
$value=0;
$nword=1;
while($line=<$fhin>){
  if($line!~/^[ *#,,]/ ){ #ignore commented lines or comma with no other char in a line
    chop $line; #remove eol
    ($fieldname,$byte,$spibits,$RW,$reset,$default,$comment)=split(",",$line);

    #figure out width from spi bits since width field in original sheet has errors
    chomp $spibits; #get rid of >
	$range=$spibits;
	$range =~ s/\[//g;
	$range =~ s/\]//g;
    if($range=~/:/){
      ($spimsb,$spilsb)=split(":",$range);
      $width=$spimsb-$spilsb+1;
    }
    else{
      $width=1;
    }

	#print "DEBUG1 $spibits msb=$spimsb lsb=$spilsb \n";
	#if($spimsb<32 && $spimsb!=""){
	if($spimsb<32){
		if($dec){
		$valueDec=$default;
		}
		else {
		$valueDec=hex $default; # convert hex to decimal
		}
		$value=$valueDec*2**$spilsb + $value; # total dec value of 32 bit reg
		#print "#debug msb=$spimsb lsb=$spilsb Current=$valueDec Total=$value\n";
	}
	$word=$value;	
	# begin of reg word
    if($spilsb==0){
		$addrDec=hex $byte;
		$baseAddrDec=hex $baseAddr;
		$addr=$baseAddrDec + $addrDec; # full address in decimal
	}
	# end of reg word
    if($spimsb==31){
		$value=0;
		$nword=$nword+1;
		printf "$SPICMD 0x%.8x 0x%.8x \n",$addr,$word;
  		printf "mdelay 50\n";
		#print "DEBUG: $spimsb range=$range width=$width $nword $SPICMD $addr $word \n";
		$spimsb="", $spilsb="";
	}
	
    #calculate current msb and lsb
    $msb=$bankbit+$width-1;
    $lsb=$bankbit;
	}
}
close $fhin;

###################
sub printfield{
  my ($fieldname,$msb,$lsb,$byte,$default,$description)=@_;
  printf "$SPICMD,\[$msb:$lsb\],0x%x,r\/w,0x0,0x%x,$description\n",$byte,$default;
}

