## program daaf from files
import argparse
import sys
import os
print '# This file is created by ' + str(sys.argv[0:])
print '# Current path is ' + os.getcwd()

parser = argparse.ArgumentParser(description="Generate SPI code to program DAAF", formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument('-w','--wifi', action='store_true', help='wifi mode')
args = parser.parse_args()
#print args
add0='0x00030000'
add1='0x00031000'
add2='0x00032000'
add3='0x00033000'
if args.wifi:
	FILE1B='wifi_lutneg.txt'
	FILE1A='wifi_lutpos.txt'
	print '#WIFI mode'
else:
	FILE1B='lte_lutneg.txt'
	FILE1A='lte_lutpos.txt'
	print '#LTE mode'

def dec2hex(n):
    """return the hexadecimal string representation of integer n"""
    return "0x%0.8X" % n

def hex2dec(s):
    """return the integer value of a hexadecimal string s"""
    return int(s, 16)

# read in wifi lock up table
fp=open(FILE1A,'r')
fn=open(FILE1B,'r')

j=0
print "# program DAAF"
print "#source files:" + FILE1A + ' ' + FILE1B 
print "echo \"start prog daaf\""
print ' \
\nmono_read_delay 50000\
\nmono_write_delay0 50000\
\nmono_write_delay1 50000\
\n#PW_I=0x2 of timing pulse\
\niwl 0x34014 0x0002\
\n#PW_Q=0x2 of timing pulse\
\niwl 0x3402c 0x0020\
'
if args.wifi:
	print '## div_ratio=0, wifi mode'
	print 'iwl 0x34030 0x00000000'
else:
	print '## div_ratio=1, LTE mode'
	print 'iwl 0x34030 0x08000000'
print '\
\necho "# enable to write SRAM"\
\niwl 0x3403c 0x001f ## write SRAM'
	
j=0
index=0
for line in fp:
	add0New = hex2dec(add0) + 4*j
	add2New = hex2dec(add2) + 4*j
	if index == 0:
		w0 = int(line)
		index = 1
	else :
		w1 = int(line)
		word16 = dec2hex(w1*2**16+w0)
		print 'echo \"pos: add byte offset:' + str(j*4) + '"'
		CMD0 = 'iwl ' + dec2hex(add0New) + ' ' + word16 
		CMD1 = 'iwl ' + dec2hex(add2New) + ' ' + word16 
		print CMD0
		print CMD1
		index=0;
		j=j+1;

print 'echo "programming neg"'
print '#last address:' + dec2hex(add0New)
j=0
index=0
for line in fn:
	#print str(j)
	add1New = hex2dec(add1) + 4*j
	add3New = hex2dec(add3) + 4*j
	if index == 0:
		w0 = int(line)
		index = 1
	else :
		w1 = int(line)
		word16 = dec2hex(w1*2**16+w0)
		print 'echo \"neg: add byte offset:' + str(j*4) + '"'
		CMD0 = 'iwl ' + dec2hex(add1New) + ' ' + word16 
		CMD1 = 'iwl ' + dec2hex(add3New) + ' ' + word16 
		print CMD0
		print CMD1
		index=0;
		j=j+1;

print '# final address offset '+ str(j-1)
print '#last address:' + dec2hex(add1New)
print '\
## stop writing memory\
\niwl 0x3403c 0x00010000\
\necho \"release analog resetb\"\
'
print "echo \"end prog daaf\""

