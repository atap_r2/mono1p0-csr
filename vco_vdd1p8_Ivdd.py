"""
using Python to set values on M8190A and read EVM from running copy of 89601B sofwtare

with major help from Kevin Klug
Author: Gennady Farber

klug@google.com
"""

from datetime import datetime
#import matplotlib.pyplot as plt
import numpy as np
#import sys
import time
import csv
#import visa
import os
import keysight.command_expert as kt
import sys
sys.path.insert(0, 'C:\Mindspeed\Scripts\MindspeedControl')
from mono_spi import Mono_SPI
mono = Mono_SPI('COM6')

def dec2hex(n):
    """return the hexadecimal string representation of integer n"""
    return "0x%0.8X" % n

def hex2dec(s):
    """return the integer value of a hexadecimal string s"""
    return int(s, 16)

#mono.send_cmd('rl 0x00')
chip = 'B1' ; # this is number on the board
os.chdir('C:\R2SiliconTest\Automation\SCPI_SEQ')
AMPL = np.linspace(-10,-3,8)
N=15;
vdc = np.zeros(N)
vdcVCO = np.zeros(8)
vac = np.zeros(N)
step  = 4
#Pout=np.zeros(len(AMPL))
#ACP_Ref=np.zeros(len(AMPL))
#ACP_Lo=np.zeros(len(AMPL))
#ACP_Up=np.zeros(len(AMPL))
## put the chip in default states, all components are powered off
mono.send_file('C:\R2SiliconTest\CSR\mono1p0CSR_def_delay_short.spi')
# turn top antest switch
mono.send_file('C:\R2SiliconTest\CSR\\alogic_SynOn_antestOn.spi')
mono.send_file('C:\R2SiliconTest\CSR\\syn_On.spi')
[vdc[1]] = kt.run_sequence('DMM25_VDC')
[idc[1]] = kt.run_sequence('DMM24_IDC')
mono.send_file('C:\R2SiliconTest\CSR\\syn_Off.spi')
[vdc[2]] = kt.run_sequence('DMM25_VDC')
[idc[2]] = kt.run_sequence('DMM24_IDC')

mono.send_file('C:\R2SiliconTest\CSR\\alogic_AllOn_antestOn.spi')
mono.send_file('C:\R2SiliconTest\CSR\\syn_On.spi')
[vdc[3]] = kt.run_sequence('DMM25_VDC')
[idc[3]] = kt.run_sequence('DMM24_IDC')
mono.send_file('C:\R2SiliconTest\CSR\\syn_Off.spi')
[vdc[4]] = kt.run_sequence('DMM25_VDC')
[idc[4]] = kt.run_sequence('DMM24_IDC')

vco_x= np.linspace(0,3,4)
fileName = chip + '_antest_dc_vco_%s.txt' %(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
np.savetxt(fileName, np.transpose([vco_x, vdc, idc]) , fmt='%.4e', delimiter='\t', header='VDD voltage at VCO and VDD current with VCO on and off')
#time.sleep(10)
mono.close()
