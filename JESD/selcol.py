# -*- coding: utf-8 -*-
# selecting columns in a multi column file
# column 2 is index 1
import sys
def selcol(INFILE, COL=1):
	import matplotlib.pyplot as plt
	import numpy as np
	import re
	VERBOSE=0
	f2 = open(INFILE, 'r')
	# read the whole file into a single variable, which is a list of every row of the file.
	lines = f2.readlines()
	f2.close()

    # scan the rows of the file stored in lines, and put the values into some variables:
	for line in lines:
        #remove leading and trailing whitespce
		linetmp = line.strip()
        # filter out comment start #
		if not linetmp.startswith("#"):
            # if first column is a number
			match=re.search('^[-0-9.]',linetmp)
			matchWhite=re.search('\s',linetmp)
			if match:            
				p = linetmp.split()
				col = int(COL)-1
				print float(p[0]), float(p[col])
			elif matchWhite:
				print('Non-number line found in file')
				print("#",linetmp)
				matchWhite=False #  reset     
			else:
				print 'unexpected characters'
		else:	
			print linetmp

	#y = np.transpose(np.array([xv,yv]))
    
def main():
	selcol(sys.argv[1],sys.argv[2])	

if __name__ == "__main__":
	main()




