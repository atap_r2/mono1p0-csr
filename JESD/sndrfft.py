#!/usr/bin/python
def sndrfftSD(data, fin=1, fs=1000, FScale=1, Nhd=7, level=-60, i=1, useWindow=1, OSR=1, doPlot=1, Navg=1, yscale=1):

#def [ SNDR_fft THD SFDR HDdbc spurDB SNR_fft x PSD Aout ] = sndrfftSD(data, fin, fs, FScale, Nhd, level, i, useWindow, OSR, doPlot, Navg, yscale)
	'''
	  function [ SNDR_fft THD SFDR HDdbc spurDB SNR_fft x PSD Aout ] = sndrfft(data, fin, fs, FScale, Nhd, level, i, useWindow, OSR, doPlot, Navg, yscale)
	 
	 Find SNR, THD etc. from using FFT. 
	 Plot FFT.

	 OUTPUTS:
	#   1 SNDR
	#   2 THD
	#   3 SFDR
	#   4 HDdbc : HDs in dBc
	#   5 spurDB: interleaved spur normalized to FS
	#   6 SNR
	#   7 freq
	#   8 FFT mag in db
	#   9 Input signal amplitude, a fraction of Full Scale
	#
	# INPUTS:
	#   1 data     : column vector containing data
	#   2 fin      : signal freq
	#   3 fs       : sampling frequency
	#   4 FScale   : default: amplitude=1
	#   5 Nhd      : order of HDs to search for, Default = 7
	#   6 level    : At what power level (dBFS) the spur to label, default = -60dB
	#   7 i        : interleave factor. Default = 1
	#   8 window   : window for fft, default=rectangular;  1: kaiser window
	#   9 OSR      : eg. 64, 128 etc default=1, For oversampled ADC, SNDR is correct but SNR number is incorrect. 
	#   10 doPlot  : 1: plot spectra 0: do not 
	#   11 Navg    : number of average in FFT domain. Enable to see small tones
	#   12 yscale  :  0: dBFs; 1: dBVrms
	#
	#   sndrfftSD(code)
	#   sndrfftSD(code,fin)
	#   sndrfftSD(code,fin,fs)
	#   sndrfftSD(code,fin,fs,FScale)
	#   sndrfftSD(code,fin,fs,FScale,Nhd)
	#   sndrfftSD(code,fin,fs,FScale,Nhd,LabelSpurLevel)
	#  e.g. 1:
	#   y = sine(1,16,0,1,2^17,0.01,0); % fin=1, fs=16, 2^17 sample, noise_sigma=0.01
	#   fin=1;fs=16;
	#   sndrfftSD(y, fin, fs, 2, 7, -60, 1, 1) ; % FScale=2; find spurs down to -60dBFS, interleave factor=1, window=Kaiser;
	#  e.g. 2: 
	#   y = sine(1,16,0,1,2^17,0.01,0); % fin=1, fs=16, 2^17 sample, noise_sigma=0.01
	#   sndrfftSD(y(:,2),1,16,1,7,-90,1,1,1,1,1,1); % use window,OSR=1,interleave=1,dBVrms, label spur down to -90dBVrms, find up to 7th HDs
	#   sndrfftSD(y(:,2),1,16,1,7,-90,1,1,128,1,1,0); % use window,OSR=128,interleave=1,dBFS
	#   [SNDR_fft THD SFDR HDdbc spurDB SNR_fft x PSD Aout]=sndrfftSD(y(:,2),1,16,1,7,-90,1,1,128,1,16,0); % use window,OSR=128,interleave=1,dBFS, 16 Avg of FFT, NFFT=N/16
	#  updated on 7/21/2016
	'''
	import sys
	import numpy as np
	from numpy import log10,linspace,floor
	from matplotlib.pyplot import axis,text,savefig,plot,show,grid,title,figure,xlabel,ylabel,subplot,legend
	[sr,sc] = data.shape;
	#FID = open('sndrfft_out.txt','w');
	#diary FID;
	alpha=280; beta=0.11*(alpha-8.7) ;
	#kwin=kaiser(sr,beta);
	spurDB=0;
	if sc == 1:
		warning('Single column data.');
		code = data;
	if sc >= 2:
		code = data[:,1]; #column index 1
		fs = 1/float(data[1,0]-data[0,0]); #column index 0
		print('2nd column is used as the data.');
	if useWindow == 1:
		kwin=np.kaiser(sr/Navg,beta);
		window = kwin; #% use kaiser window
		Nbin_1_f=12 #% 1/f noise bin
		kwinPk=-12.8; #% peak of kaiser window, in dB
	else:
		window = 1;
		Nbin_1_f=10 #% 1/f noise bin
		kwinPk=0; #% peak of rec window, in dB
	if yscale:
		FScale=FScale*np.sqrt(2)
	else:
		pass
	FpPwr=0;
	N = float(len(data));
	#print(N)
	HDpwr = 0;
	RBW= fs/N/Navg; #% resolution BW
#window = chebwin(length(code),120);
# DEFAULT: FScale = 1, am amp = 1 sinewave appear at 0 dB
	N = N/Navg;
	normFac = FScale/1 * 10**(kwinPk/20);
	for iavg in range(1,Navg+1):
		codeX=code[(iavg-1)*N:(iavg)*N];
		#print(codeX-np.mean(codeX))
		#print(type(window))
		F = np.fft.fft((codeX-np.mean(codeX))*window);
		Fmax = max(abs(F[0:np.floor(N/2/OSR)])); #% signal bin
		xmax = np.argmax(abs(F[0:np.floor(N/2/OSR)])); #% signal bin
		Fout = xmax/N*fs;
		#%Fp = abs(F(0:floor(length(code)/2)+1))/(N/2)*2; #% extra 2 due to windowing
		Fptemp = abs(F[0:np.floor(N/2)])/(N/2);
		FpPwr = Fptemp**2+FpPwr;    #% power adds

	Fp = (FpPwr/Navg)**(0.5);
	PSD = 20*log10(Fp+1e-9) - 20*log10(normFac); #%% normalized to full scale
	Pmax = max(PSD);
	Pxmax = np.argmax(PSD)
	x = linspace(0,fs/2-fs/N,N/2,endpoint=True);
	#print x.shape
	#print Fp.shape,xmax
	#plot(x,PSD)
	#show()
	HD=np.zeros(Nhd+1)
#% search for Harmonics
	for j in range(1,Nhd+1):
	   xhd = np.mod(j*xmax,N); # pyton array index start with 0, so it is not (xmax-1)
	   xhd = min(xhd,N-1-xhd); #% xhd is below Fs/2
	   if j > 10:
		  if floor(len(Fp)/1000) > xhd+Nbin_1_f:
				HD[j] = 1e-9;
		  else:
				HD[j] = max(Fp[max(xhd-16,floor(len(Fp)/1000)):min(xhd+16,len(Fp)-5)]);
	   else:
			#% if xhd is outside 1 and Fs
			bin1 = max(xhd-Nbin_1_f,1); 
			bin2 = min(xhd+Nbin_1_f,N/2);
			#print j,(xhd-1)/float(N)*fs 
			HD[j] = max(Fp[bin1:bin2]);
			if OSR == 1:
			  HDpwr = HDpwr + sum( Fp[bin1:bin2] ** 2 ) ; #% calculate sum of Fund. and Harmonics power
			else:
				if xhd < N/OSR/2:
					HDpwr = HDpwr + sum( Fp[bin1:bin2] ** 2 ) ; #% calculate sum of Fund. and Harmonics power

	   HDdb = 20*log10(HD[j])- 20*log10(normFac); #% norm to Full Scale
	   HDdbc = 20*log10(HD[j])-20*log10(HD[1]); #% dBc
	   print('HD%d at %.6g Hz at %.4g dBFS'%(j,xhd/N*float(fs),HDdb));
	   Aout=10**((20*log10(HD[1])- 20*log10(normFac))/20);
	   #%%% fundamental
	   if j == 1:
			text(xhd/N*fs, HDdb, 'Fin: %2.1f' %HDdb,rotation=45); 
	   elif j > 1 and HDdb > level:
			text(xhd/N*fs, HDdb, 'HD%d %2.1f' %(j, HDdb), rotation=45 );
		#%% search for interleave error. Fs+-Fin, Fs+-HD2, Fs+-HD3
	   for M in range(1,int(floor(i/2))+1):
			for p in [-1,1]:
				x_ispur = mod(p*xhd + M*N/i,N); #% for  > (+-fin+ fs/i) produce same freq 
				x_ispur = min(x_ispur,N-x_ispur)+1;
				spurMag = max(Fp[max(x_ispur-Nbin_1_f,1):min(x_ispur+Nbin_1_f,len(Fp))]);
				spurDB = 20*log10(spurMag) - 20*log10(normFac); #% normalzed to full scale
			if spurDB > level:
				text(x_ispur/N*fs, spurDB, '%d*HD+Fs*%d/%d(%2.1fdBFS)' %(j,M,i,spurDB), rotation=45,hold=1); 
				print('Interleave Spur %d*%d*fin+%d/%d*Fs at %.4g at %.4g dB\n' %(p,j,M,i,x_ispur/N*fs,spurDB))

	THD = -10*log10(HD[1]**2/sum(HD[2:Nhd+1]**2));
	HDmax = max(20*log10(HD[2:])-20*log10(normFac)); #%% normalized to full scale
	xhd = np.argmax(20*log10(HD[2:])-20*log10(normFac)); #%% normalized to full scale

	#%% summarize results
	if ( xmax > Nbin_1_f ):
		signal = Fp[xmax-Nbin_1_f:xmax+Nbin_1_f];
	else:
		signal = Fp[xmax:xmax+Nbin_1_f];

	if xmax > len(Fp)/OSR: #% if signal is outside the intended BW.
	  SNDR_fft = 10*log10(sum(signal**2)/(sum(Fp[0:floor(N/OSR)]**2)-sum(Fp[0:Nbin_1_f]**2)));
	  SNR_fft = 10*log10(sum(signal**2)/(sum(Fp[0:floor(N/OSR)]**2)-sum(Fp[0:Nbin_1_f]**2))); #% could have problem
	else:
	  SNDR_fft = 10*log10(sum(signal**2)/(sum(Fp[0:floor(N/OSR)]**2)-sum(Fp[0:Nbin_1_f]**2)-sum(signal**2)));
	  SNR_fft = 10*log10(sum(signal**2)/(sum(Fp[0:floor(N/OSR)]**2)-sum(Fp[0:Nbin_1_f]**2)-HDpwr));

	#SNDR_fft = 10*log10(sum(signal**2)/(sum(Fp**2)-Fp(1)^2-sum(signal**2)));
	#SNR_fft = 10*log10(sum(signal**2)/(sum(Fp**2)-Fp(1)^2-HDpwr)); % doesn't work oversampled adc
	ENOB_fft = (SNDR_fft-1.76)/6.02;
	SFDR = max(max(PSD[floor(len(PSD)/100):xmax-16]),max(PSD[xmax+16:N/OSR/2])); #% in dBFS
	if SNR_fft < SNDR_fft:
		warning('SNR < SNDR')
	if SFDR >= HDmax+0.1:
	    print('Warning: Max spur occurs outside of searched HDs.');
	SFDR = -(max(SFDR,HDmax)-20*log10(HD[1]/normFac)); #% in dBc
	print('SNDR = %.4g (dB), SNR = %.4g (dB), THD = %.4g (dB)  SFDR = %.4g (dB)\n'%(SNDR_fft, SNR_fft, THD, SFDR));
	s = 'SNDR = %.4gdB, SNR = %.4gdB, ENOB = %.3g, THD = %.4gdB \n SFDR = %.4gdB, Fin = %.4g, Fs = %.4g, %d-pt,RBW=%.3gHz'%(SNDR_fft,SNR_fft,ENOB_fft,THD,SFDR,Fout,fs,N,RBW);
	if doPlot == 1:
		if OSR == 1: 
		   plot(x,PSD); 
		   #axis([x[0], x[-1], -100, 0]);
		   grid();
		   #ylabel('Mag [dBFS]'); xlabel('Frequency[Hz]');
			
		else:
		   max(x);
		   max(PSD);
		   semilogx(x,PSD); 
		   #%axis([x(2) x(end)/OSR*2 -150 0]);
		   #%ylabel('Mag [dBFS]'); xlabel('Frequency[Hz]'); title(s); hold off;
		   text(100,-20,'OSR = %.4g'%OSR);
	if yscale == 0:
		ylabel('Mag [dBFS]'); xlabel('Frequency[Hz]'); title(s); 
	else:
		ylabel('Mag [dBVrms]'); xlabel('Frequency[Hz]'); title(s); 
	show();
def main(args):
	import twoCol2Array as tca
	data=tca.twoCol2Array(args.file)
	print(data)
	#print(data.shape)
	sndrfftSD(data,fs=args.Fsamp,level=args.Level,FScale=args.FScale,Nhd=args.Harm,yscale=args.yscale,i=args.Nint,doPlot=args.doPlot,Navg=args.avg,OSR=args.osr)	

if __name__ == "__main__":
	import argparse
	import sys
	parser = argparse.ArgumentParser(description="Do FFT of the input data. Input could be two column or on column data", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	# with optional argument ?
	parser.add_argument('-f','--fin', type=float,default=1, help='Signal Frequency')
	# one or more argument +
	parser.add_argument('-F','--Fsamp', type=float, default=10,help='Sampling Frequency')
	parser.add_argument('-a','--avg', type=int, default=1,help='Number of Average in fft')
	parser.add_argument('-H','--Harm', type=int, default=7,help='h-th harmonics to label')
	parser.add_argument('-p','--doPlot', action='store_true', default=True, help='Show fft plot')
	parser.add_argument('-l','--Level', type=int, default=-60,help='Spur level (dB) to annotate on plot')
	parser.add_argument('-S','--FScale', type=float, default=1, help='Full Scale Signal')
	parser.add_argument('-y','--yscale', type=float, default=1, help='yscale of the plot')
	parser.add_argument('-w','--window', action='store_true', default=True, help='Windowing for fft, kaiser window')
	parser.add_argument('-i','--Nint', type=int,default=1, help='Interleave factor for time-interleaved ADC')
	parser.add_argument('-o','--osr', type=int, default=1,help='OSR if it is data from oversampling ADC')
	parser.add_argument("file", type=str, help="Input data file")
	#parser.print_help()
	args = parser.parse_args()
	main(args)

