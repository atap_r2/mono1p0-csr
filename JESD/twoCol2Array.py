# -*- coding: utf-8 -*-
# convert for two column file to a two-column numpy array
import sys
def twoCol2Array(INFILE, DEBUG=0):
	import matplotlib.pyplot as plt
	import numpy as np
	import re
	VERBOSE=0
	f2 = open(INFILE, 'r')
	# read the whole file into a single variable, which is a list of every row of the file.
	lines = f2.readlines()
	f2.close()
	# column 1
	x1 = []
	y1 = []

    # scan the rows of the file stored in lines, and put the values into some variables:
	for line in lines:
        #remove leading and trailing whitespce
		linetmp = line.strip()
		if VERBOSE == 1:
			print(linetmp)
        # filter out comment start #
		if not linetmp.startswith("#"):
            # if first column is a number
			match=re.search('^[-0-9.]',linetmp)
			matchWhite=re.search('\s',linetmp)
			if match:            
				p = linetmp.split()
				x1.append(float(p[0]))
				y1.append(float(p[1]))
			elif matchWhite:
				print('Non-number line found in file')
				print(linetmp)
				matchWhite=False #  reset     
			else:
				print('#white line')

	xv = np.asarray(x1)
	yv = np.asarray(y1)
	#print(type(xv))
	#print xv
	y = np.transpose(np.array([xv,yv]))
	#print(y.shape)
	if DEBUG == 1:
		print(y)
	#plt.plot(y[:,0],y[:,1])
	return y
    
def main():
	twoCol2Array(sys.argv[1],sys.argv[2])	

if __name__ == "__main__":
	#print(sys.argv[0:2])
	main()




